# Scanbook/Scripts/camera.py

from picamera import PiCamera
from time import sleep
import RPi.GPIO as GPIO

import data

def capture(camNb):
    
    '''
    Function used to take a picture with the camera
    The argument camNb is used to define the camera that will be used
    '''

    # The mux is used to select the camera 1 or 2
    if(camNb == 1):
        GPIO.output(data.muxPin, False)
    elif(camNb == 2):
        GPIO.output(data.muxPin, True)

    data.pageNb+=1

    # Instanciation of the camera
    camera = PiCamera()
    # Setting the resolution according to the value set in the data.py file
    camera.resolution = (data.camResX, data.camResY)

    # Uncomment to test and adjust the focus of the cameras
    #camera.start_preview()
    sleep(1) # adjust the preview time to your liking
    #camera.stop_preview()

    # The picture will be saved in the Scan/Captured folder with the Page# name
    # The format is done according to the value in the data.py file
    # jpg is compressed but is so much quicker and lighter
    camera.capture(data.capturedPath + '/Page' + str(data.pageNb) + data.imageFormat)

    # Don't forget to close the cam
    camera.close()
    return None

def init():

    # MUX: 0->cam?, 1->cam?
    GPIO.setup(data.muxPin, GPIO.OUT)
    return None

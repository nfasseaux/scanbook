# Scanbook/button.py

import RPi.GPIO as GPIO

import stateMachine as SM
import led
import data

def buttonStartCallback(buttonStartPin):
    SM.stateMachine(data.Event.START)
    return None

def buttonNextCallback(buttonNextPin):
    SM.stateMachine(data.Event.NEXT)
    return None

def buttonEndCallback(switchEndPin):
    SM.stateMachine(data.Event.END)
    return None

def buttonLedMinusCallback(buttonLedMinusPin):
    if data.ledDutyCycle <= 500000:
        data.ledDutyCycle = 0
    else:
        data.ledDutyCycle -= 100000
    led.pwm.hardware_PWM(data.ledPin, data.ledFreq, data.ledDutyCycle)
    return None

def buttonLedPlusCallback(buttonLedPlusPin):
    if data.ledDutyCycle != 1000000:
        data.ledDutyCycle += 100000
        led.pwm.hardware_PWM(data.ledPin, data.ledFreq, data.ledDutyCycle)
    return None

def init():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)

    # Button START: threaded callback
    GPIO.setup(data.buttonStartPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data.buttonStartPin, GPIO.FALLING, bouncetime=200)
    GPIO.add_event_callback(data.buttonStartPin, buttonStartCallback)
    
    # button NEXT: threaded callback
    GPIO.setup(data.buttonNextPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data.buttonNextPin, GPIO.FALLING, bouncetime=200)
    GPIO.add_event_callback(data.buttonNextPin, buttonNextCallback)

    # button END: threaded callback
    GPIO.setup(data.buttonEndPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data.buttonEndPin, GPIO.FALLING, bouncetime=200)
    GPIO.add_event_callback(data.buttonEndPin, buttonEndCallback)

    # button LEDMINUS: threaded callback
    GPIO.setup(data.buttonLedMinusPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data.buttonLedMinusPin, GPIO.FALLING, bouncetime=200)
    GPIO.add_event_callback(data.buttonLedMinusPin, buttonLedMinusCallback)

    # button LEDPLUS: threaded callback
    GPIO.setup(data.buttonLedPlusPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(data.buttonLedPlusPin, GPIO.FALLING, bouncetime=200)
    GPIO.add_event_callback(data.buttonLedPlusPin, buttonLedPlusCallback)

    return None

# Scanbook/Scripts/stateMachine.py

from enum import Enum
import os
import sys
import time
import pigpio
from PIL import Image
import glob
from fpdf import FPDF
import tkinter as tk
import tkinter.font as tkFont

import camera
import imageProcessing as iP
import led
import motor
import data

def stateMachine(event):
    '''
    Function that implement the state machine
    the event parameter determine the next state.
    See the report for more information about the
    different states and events.
    '''
    if data.currState == data.State.INIT:
        if event == data.Event.NONE:
            stateStart()
    if data.currState == data.State.START:
        if event == data.Event.SHUTDOWN: 
            stateShutdown()
        elif event == data.Event.START:
            stateInitScan()
    if data.currState == data.State.INITSCAN:
        if event == data.Event.NONE:
            stateScan()
    if data.currState == data.State.SCAN:
        if event == data.Event.NONE:
            stateNext()
    if data.currState == data.State.NEXT:
        if event == data.Event.NEXT:
            stateScan()
        elif event == data.Event.END:
            stateSave()
    if data.currState == data.State.SAVE:
        if event == data.Event.MENU:
            stateStart()

def stateInit():
    '''
    Initialisation state of the SM.
    We only pass here once at the start.
    '''
    # Change the current state
    data.currState=data.State.INIT
    #print('     STATE INIT')
    
    # Set the GUI to the windowInit
    mainFrame.windowInit()

    # light the LED at max brightness
    led.setPwm(1000000)
    # Engage to motor to lift the pod
    motor.up()

    # Call the SM function with the corresponding event.
    # In this case, the event is none.
    # next state -> stateStart
    stateMachine(data.Event.NONE)

def stateShutdown():
    '''
    Function called at the shutdown of the SB
    We only close the script and not the all RPi
    '''
    data.currState=data.State.STOP
    #print('     STATE STOP')

    # Turn the LED OFF
    led.setPwm(0)
    # Lower the pod
    motor.down()
    # Deactivate the motor
    motor.off()

    # Deactivate the GPIO deamon
    os.system('sudo killall pigpiod')

    # Kill the process
    sys.exit(0)

def stateStart():
    '''
    Second state of the SM
    Only changed the GUI and display the main menu
    '''
    data.currState=data.State.START 
    #print('     STATE START')

    # Change the GUI
    mainFrame.menuStart()

def stateInitScan():
    '''
    State to init the scan:
    create the dir for the book and the sub dir (captured & traited)
    '''
    data.currState=data.State.INITSCAN
    #print('     STATE INITSCAN')

    # Get the time for the book name
    localTime = time.localtime(time.time())
    # Set the book name to yy-mm-ss-hh:mm:ss
    data.bookName = str(localTime[0]) + "-" + str(localTime[1]) + "-" + str(localTime[2])+ "-" + str(localTime[3])+ "-" + str(localTime[4]) + "-" + str(localTime[5])

    # Get the current working directory path
    pathCwd = os.getcwd()
    
    # Check if the Scan Dir already exist and create it if not
    scanDir = False
    rootDirList = glob.glob(pathCwd + '/../*')
    for rootDir in rootDirList:
        if rootDir == (pathCwd + '/../Scan'):
            scanDir = True
    if scanDir == False:
        os.mkdir(pathCwd + '/../Scan', data.accessRights)
    
    # Set the different useful path
    data.bookPath = pathCwd + '/../Scan/' + data.bookName
    data.capturedPath = data.bookPath + '/Captured'
    data.traitedPath = data.bookPath + '/Traited'
    
    # Create the needed directory
    os.mkdir(data.bookPath, data.accessRights)
    os.mkdir(data.capturedPath, data.accessRights)
    os.mkdir(data.traitedPath, data.accessRights)

    # Call the SM function the event none
    # next state -> stateScan
    stateMachine(data.Event.NONE)

def stateScan():
    '''
    State function to performe a complete scan
    '''
    data.currState=data.State.SCAN
    #print('     STATE SCAN')

    # Change the GUI
    mainFrame.windowMoveDown()

    # Lower the pod
    motor.down()

    # Change the GUI
    mainFrame.windowTakingPictures()
    # Capture left page
    camera.capture(1)
    # Traite left page
    iP.borderRecognition(data.pageNb)
    # Capture right page
    camera.capture(2)
    # Traite right page
    iP.borderRecognition(data.pageNb)

    # Change GUI
    mainFrame.windowMoveUp()
    # Lift the pod
    motor.up()

    # Call the SM with the none event
    # next state -> stateNext
    stateMachine(data.Event.NONE)

def stateNext():
    '''
    State function that only display the next menu
    '''
    data.currState=data.State.NEXT
    #print('     STATE NEXT')

    # change GUI
    mainFrame.menuNext()

def stateSave():
    '''
    State that only display the save menu GUI
    '''
    data.currState=data.State.SAVE
    #print('     STATE END')

    # Change to GUI
    mainFrame.menuSave()

def save2usb():
    '''
    Function to combine of the pages of the scanned book into one pdf
    and save it on a USB stick
    '''
    # Save the pages path in a list
    imageList=glob.glob(data.traitedPath + '/*.jpg')
    # Sort the list otherwise the page won't be in order in the pdf
    imageList.sort()
    # Set the margin to 10, the margin is optional and could be 0
    margin=10
    # Set the size from the first page
    cover = Image.open(imageList[0])
    width, height = cover.size
    # Creating the pdf
    pdf = FPDF(unit='pt', format=[width + 2*margin, height + 2*margin])
    # Adding every other page to the pdf
    for imagePath in imageList:
        pdf.add_page()
        pdf.image(imagePath, margin, margin)
    pdf.output(data.bookPath + '/' +data.bookName + '.pdf', 'F')

    # Getting the path of all the peripheral connected to the RPi
    listMedia = glob.glob('/media/pi/*')
    # Removing from the list the "SETTINGS" folders containing
    # settings parameter for the different USB port
    listMedia.remove('/media/pi/SETTINGS1')
    listMedia.remove('/media/pi/SETTINGS2')
    listMedia.remove('/media/pi/SETTINGS3')
    listMedia.remove('/media/pi/SETTINGS4')
    # Since the USB is the last peripheral that has been connected that's how we find it
    latestMedia = min(listMedia, key=os.path.getctime)
    
    # Checking if there is already a ScanBook dir and if not we create one
    dirList = glob.glob(latestMedia + '/*')
    scanbookDir = False
    for directory in dirList:
        if directory == (latestMedia + '/ScanBook'):
            scanbookDir = True
    if scanbookDir == False:
        os.mkdir(latestMedia + '/ScanBook', data.accessRights)

    # Save the pdf to the ScanBook dir on the USB stick
    os.system('sudo cp ' + data.bookPath + '/' + data.bookName + '.pdf ' + latestMedia + '/ScanBook/' + data.bookName + '.pdf')

    # Here we could delete the file saved on the Raspberry Pi but it's better to keep them for the report and debug


###########################################################################################################

class ScanBookGUI(tk.Frame):
    
    def __init__(self, master):
        '''
        Init function for the class. Called each time a new class object is created. 
        In our case, it's only one.
        '''
        # we create a attribut master that will be assigned to master.
        # The master is the window of the GUI.
        self.master = master

        # We set the size of the window to the size of the screen -> fullscreen
        master.geometry('{0}x{1}+0+0'.format(self.master.winfo_screenwidth(), master.winfo_screenheight()))
        # We give a name to the window
        master.title('ScanBook')
        # Command to set the window in a 'real' fullscren -> no close button or top border
        # Can be deactivate during the developpement process
        master.overrideredirect(1)

        # Creating a attribut called frame
        # Every widget of the GUI( button, label,...) will then be put inside that frame
        # We have a widget inside a frame inside a window
        self.frame = tk.Frame(self.master)

    def windowInit(self):
        '''
        Function called to set the frame with the desired GUI widget
        '''
        # We first delete everything that was in the frame
        # That way, we can start from a clean sheet
        self.frame.destroy()

        # Resetting the frame parameter
        # We use pack as geometry managed to center the frame to the right size inside the window
        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        # We use the rowId variable to keep track of the row id
        # We always start at the first one and then move on to the next one
        # That way, we can add a row in the middle on the fly and don't need to redo all the row index
        rowId = 0
        
        # Setting the column as we wan
        # Here we want 5 column of equal width
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        
        # Setting the first row to be blank
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        # Same for the second one
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        # Idem
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        # Setting a Label inside the 4th row (rowId=3)
        # Creating a label inside the frame with myFont
        self.label1 = tk.Label(self.frame, text='Initialization...', font=myFont)
        # Setting the label inside the frame with the grid geometry manager
        # The sticky parameter is to define the position of the widget inside the row and column
        # Here we say to sticky est and west, so it will stretch horizontaly to button
        self.label1.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1
        
        # Again, the other rows are left blank
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        # We update the window with the new frame
        self.master.update()

    def menuStart(self):
        '''
        Tkinter definition for the menuStart GUI
        '''
        self.frame.destroy()

        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        
        rowId = 0

        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        self.frame.grid_columnconfigure(5, weight=1)

        # Code used to resize the png that will be on the button
        # uncomment to resize, comment if already done
        #im = Image.open("Images/LogoLightBulb.png")
        #im = im.resize((40, 40), Image.ANTIALIAS)
        #im.save("Images/LogoLightBulbResize.png")
        # Creating a button in the top right corner with a logo
        self.logo = tk.PhotoImage(file='Images/LogoLightBulbResize.png')
        # The button is set to call the buttonLight method when pressed
        self.button_light = tk.Button(self.frame, image=self.logo, command=self.buttonLight)
        # The button paramater are set according to the value in the data.py file
        self.button_light.config(height=data.buttonLightHeight, width=data.buttonLightWidth)
        self.button_light.grid(row=rowId, column=5, sticky="e")
        rowId += 1

        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.label = tk.Label(self.frame, text='Welcome to the ScanBook', font=myFont)
        self.label.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1

        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        # We defined the action of the pressed in a lambda function beacause we need to pass an argument
        self.button_start = tk.Button(self.frame, text='START', command= lambda: stateMachine(data.Event.START))
        self.button_start.config(height=data.buttonHeight, width=data.buttonWidth)
        self.button_start.grid(row=rowId, column=2)
        self.button_shutdown = tk.Button(self.frame, text='SHUTDOWN', command= lambda: stateMachine(data.Event.SHUTDOWN))
        self.button_shutdown.config(height=data.buttonHeight, width=data.buttonWidth)
        self.button_shutdown.grid(row=rowId, column=4)
        rowId += 1 
        
        self.frame.grid_rowconfigure(rowId, weight=1)

        self.master.update()

    def buttonLight(self):
        '''
        Function called at the press of the light button
        Cycle the led PWM duty cyle from 50% to 100% with 10% step
        '''
        if data.ledDutyCycle >= 1000000:
            data.ledDutyCycle = 500000
        else:
            data.ledDutyCycle += 100000
        led.pwm.hardware_PWM(data.ledPin, data.ledFreq, data.ledDutyCycle)
        return None

    def windowMoveDown(self):
        self.frame.destroy()
        
        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        rowId = 0
        
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.label1 = tk.Label(self.frame, text='Scanning...', font=myFont)
        self.label1.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1

        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.label2 = tk.Label(self.frame, text='The ScanBook is moving down', font=myFont)
        self.label2.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.master.update()

    def windowTakingPictures(self):
        self.frame.destroy()
        
        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        rowId = 0
        
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.label1 = tk.Label(self.frame, text='Scanning...', font=myFont)
        self.label1.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.label2 = tk.Label(self.frame, text='Taking pictures', font=myFont)
        self.label2.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.master.update()

    def windowMoveUp(self):
        self.frame.destroy()
        
        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        rowId = 0
        
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.label1 = tk.Label(self.frame, text='Scanning...', font=myFont)
        self.label1.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.label2 = tk.Label(self.frame, text='The ScanBook is moving up', font=myFont)
        self.label2.grid(columnspan=5, row=rowId, sticky = (tk.E, tk.W))
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.master.update()
        
    def menuNext(self):
        self.frame.destroy()
        
        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        rowId = 0
        
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        self.frame.grid_columnconfigure(5, weight=1)
        
        self.logo = tk.PhotoImage(file='Images/LogoLightBulbResize.png')
        self.button_light = tk.Button(self.frame, image=self.logo, command=self.buttonLight)
        self.button_light.config(height=data.buttonLightHeight, width=data.buttonLightWidth)
        self.button_light.grid(row=rowId, column=5, sticky="e")
        rowId += 1
        
        self.label1 = tk.Label(self.frame, text='Page '+str(data.pageNb-1)+' and '+str(data.pageNb)+' had been scanned', font=myFont)
        self.label1.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.label2 = tk.Label(self.frame, text='To scan next pages, press NEXT', font=myFont)
        self.label2.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1
        
        self.label3 = tk.Label(self.frame, text='To save the book, press END', font=myFont)
        self.label3.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.button_next = tk.Button(self.frame, text='NEXT', command= lambda: stateMachine(data.Event.NEXT))
        self.button_next.config(height=data.buttonHeight, width=data.buttonWidth)
        self.button_next.grid(row=rowId, column=2)
        self.button_end = tk.Button(self.frame, text='END', command= lambda: stateMachine(data.Event.END))
        self.button_end.config(height=data.buttonHeight, width=data.buttonWidth)
        self.button_end.grid(row=rowId, column=4)
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.master.update()

    def menuSave(self):
        self.frame.destroy()
        
        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        rowId = 0
        
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(4, weight=1)
        self.frame.grid_columnconfigure(5, weight=1)
        
        self.logo = tk.PhotoImage(file='Images/LogoLightBulbResize.png')
        self.button_light = tk.Button(self.frame, image=self.logo, command=self.buttonLight)
        self.button_light.config(height=data.buttonLightHeight, width=data.buttonLightWidth)
        self.button_light.grid(row=rowId, column=5, sticky="e")
        rowId += 1
        
        self.label1 = tk.Label(self.frame, text='Scan complete', font=myFont)
        self.label1.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.label2 = tk.Label(self.frame, text='To save to an USB stick, press USB', font=myFont)
        self.label2.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1
        
        self.label3 = tk.Label(self.frame, text='To go back to the starting menu, press MENU', font=myFont)
        self.label3.grid(columnspan=6, row=rowId, sticky="nsew")
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1
        
        self.button_usb = tk.Button(self.frame, text='USB', command=save2usb)
        self.button_usb.config(height=data.buttonHeight, width=data.buttonWidth)
        self.button_usb.grid(row=rowId, column=2)
        self.button_menu = tk.Button(self.frame, text='MENU', command= lambda: stateMachine(data.Event.MENU))
        self.button_menu.config(height=data.buttonHeight, width=data.buttonWidth)
        self.button_menu.grid(row=rowId, column=4)
        rowId += 1
        
        self.frame.grid_rowconfigure(rowId, weight=1)
        rowId += 1

        self.master.update()        

# Declaration the window for the GUI
root = tk.Tk()

# Declaration of the font
# Should probably be in the data file, but osef
myFont = tkFont.Font(family='Arial', size = 24, weight="bold")

# Launching the GUI
mainFrame = ScanBookGUI(root)

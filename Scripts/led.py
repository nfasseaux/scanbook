# Scanbook/Scripts/led.py

import pigpio

import data

def setPwm(dutyCycle):
    '''
    Function used to change to duty cycle(=intensity) of the LED strip.
    The duty cycle go from 0 to 1 000 000.
    '''
    pwm.hardware_PWM(data.ledPin, data.ledFreq, dutyCycle)
    # Update the duty cycle value stored in the data.py file
    data.ledDutyCycle = dutyCycle
    return None

# Init of the pin with the pigpio library
pwm = pigpio.pi()
pwm.set_mode(data.ledPin, pigpio.OUTPUT)

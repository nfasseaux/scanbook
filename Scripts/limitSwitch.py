# Scanbook/Scripts/limitSwitch.py

import RPi.GPIO as GPIO

import data

def init():
    '''
    Initialisation function for the upper and lower limit switch of the ScanBook
    '''

    # Setting the pin naming convention, we are using the GPIO number and not the pin number
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)

    # Booth limit are used by waiting for an edge
    # There for they are defined as input pin and a pull up is added

    # Limit Switch UP: wait for edge
    GPIO.setup(data.switchUpPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Limit Switch DOWN: wait for edge
    GPIO.setup(data.switchDownPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
    return None

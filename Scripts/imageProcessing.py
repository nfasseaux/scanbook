# Scanbook/Scripts/imageProcessing.py

import cv2
import numpy as np
import os
import sys

import data

def transform(contour):
    '''
    This function is used to find the corners of the object and the dimensions of the object
    This shitty function is the only thing left from 2018 and sometime create bug due to 
    unreachable index but I don't have the time to rewrite it so be it.
    '''
    pts=[]
    for i in range(len(contour)):
            pts.append(list(contour[i][0]))
    sums={}
    diffs={}
    tl=tr=bl=br=1
    for i in pts:
        x=i[0]
        y=i[1]
        sum=x+y
        diff=y-x
        sums[sum]=i
        diffs[diff]=i
    sums=sorted(sums.items())
    diffs=sorted(diffs.items())
    n=len(sums)
    rect=[sums[0][1],diffs[0][1],diffs[n-1][1],sums[n-1][1]]
    #	   top-left   top-right   bottom-left   bottom-right
    
    h1=np.sqrt((rect[0][0]-rect[2][0])**2 + (rect[0][1]-rect[2][1])**2)		#height of left side
    h2=np.sqrt((rect[1][0]-rect[3][0])**2 + (rect[1][1]-rect[3][1])**2)		#height of right side
    h=max(h1,h2)
    
    w1=np.sqrt((rect[0][0]-rect[1][0])**2 + (rect[0][1]-rect[1][1])**2)		#width of upper side
    w2=np.sqrt((rect[2][0]-rect[3][0])**2 + (rect[2][1]-rect[3][1])**2)		#width of lower side
    w=max(w1,w2)
    
    return int(w),int(h),rect

def rotate_bound(image, angle):
    '''
    The function is used to rotate the image by a set angle.
    Most importantly, it resize the frame of the image (doesn't cut the image)
    (see the final report for more information)
    '''
    # Grab the dimensions of the image and then determine the center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # Grab the rotation matrix (applying the negative of the
    # Angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # Compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # Adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # Perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

def borderRecognition(page):

    '''
    Function used to cut the useless part of the picture to only keep the page
    '''

    debug = False

    # Import the image to process
    imgOriginal=cv2.imread(data.capturedPath + '/Page' + str(page) + data.imageFormat)
    if debug==True:
        cv2.imshow('Original',imgOriginal)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/original.jpg', imgOriginal)
    
    if ((page%2)==1):
        imgOriginal = imgOriginal[0:1944, 600:2300]
    elif((page%2)==0):
        imgOriginal = imgOriginal[0:1944, 300:2000]
    if debug==True:
        cv2.imshow('Crop',imgOriginal)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/cropped.jpg', imgOriginal)

    imgInput = imgOriginal.copy()
    
    # Resize the image to lower the resolution
    # If other than (2592, 1944) input image, adjust accordingly
    r=4 # ratio between the resolution
    dim=(425,486) #(648, 486) # resolution after reszize
    imgInput=cv2.resize(imgInput, dim, interpolation = cv2.INTER_AREA)
    if debug==True:
        imgResized = imgInput.copy()
        cv2.imshow('Resized',imgInput)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/resized.jpg', imgInput)
    
    # Couple of filters to lower the quality in order to ease the processing
    imgInput=cv2.cvtColor(imgInput,cv2.COLOR_BGR2GRAY)
    if debug==True:
        cv2.imshow('RemovedColor', imgInput)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/colorRemoved.jpg', imgInput)
    imgInput=cv2.GaussianBlur(imgInput,(5,5),0)
    if debug==True:
        cv2.imshow('Blur', imgInput)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/blured.jpg', imgInput)
    imgInput=cv2.Canny(imgInput,100,200)
    if debug==True:
        cv2.imshow('Canny', imgInput)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/canny.jpg', imgInput)

    # Contour finding using the opencv function
    _,contours,_=cv2.findContours(imgInput,1,1)
    if debug==True:
        imgDrawContours = imgResized.copy()
        cv2.drawContours(imgDrawContours,contours,-1,[0,255,0],2)
        cv2.imshow('Contours', imgDrawContours)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/contours.jpg', imgDrawContours)
    periMax=0
    # The loop is to find the longest contour, which is more than probably the page
    for contour in contours:
            peri=cv2.arcLength(contour, True)
            if peri>periMax:
                    periMax = peri
                    contourLongest = contour
    if debug==True:
        imgDrawContourLongest = imgResized.copy()
        cv2.drawContours(imgDrawContourLongest,contourLongest,-1,[0,255,0],2)
        cv2.imshow('ContourLongest', imgDrawContourLongest)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('Debug/contourLongest.jpg', imgDrawContourLongest)

    # Mapping the contour of the lower res onto the original
    contourLongest=r*np.array(contourLongest)
    contourLongestApprox=cv2.approxPolyDP(contourLongest,0.02*periMax,True)

    # If the scan is bad (no book, very weird page, whatever,..)
    # There could be no valid contour -> not big enough to be a page
    if periMax < 500:
        print('Error, no page contour found')
    else:
        try:
            w,h,arr=transform(contourLongestApprox)
        
            # warp function from previous group, not commented, like everything....
            pts2=np.float32([[0,0],[w,0],[0,h],[w,h]])
            pts1=np.float32(arr)
            M=cv2.getPerspectiveTransform(pts1,pts2)
            imgInput=cv2.warpPerspective(imgOriginal,M,(w,h))

            # Those two seems to do shit so I commented them
            # because there is no need to lower the quality/color of the final image
            #image = cv2.cvtColor(dst,cv2.COLOR_BGR2GRAY)
            #image=cv2.adaptiveThreshold(image,255,1,0,11,2)
            
            # Cut the image to only what we want
            imgInput = cv2.resize(imgInput, (w,h), interpolation = cv2.INTER_AREA)
       
            # Rotate the image, picture from the 1st cam are rotate 90 and picture from 
            # the 2s are rotated -90
            if ((page%2) == 1):
                imgFinale = rotate_bound(imgInput, 90)
            elif ((page%2) == 0):
                imgFinale = rotate_bound(imgInput, -90)

            # Saving of the traited image int the rep Scan/Traited
            cv2.imwrite(data.traitedPath + '/Page' + str(page) + '.jpg', imgFinale)

            if debug==True:
                cv2.imshow('Final', imgFinale)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
                cv2.imwrite('Debug/final.jpg', imgFinale)
        
        except:
            print('error in transfom')


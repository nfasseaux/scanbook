#!/usr/bin/env python3

# ScanBook/Scripts/main.py

import os

# Launch the deamon for the GPIO
os.system('sudo pigpiod')

import signal
import pigpio

import limitSwitch
import camera
import motor
import stateMachine as SM

# Initialisation of all the component of the ScanBook
limitSwitch.init()
camera.init()
motor.init()

# Launching the state machine
SM.stateInit()

# Lauching the GUI
SM.root.mainloop()

#Scanbook/Scripts/motor.py

import RPi.GPIO as GPIO
import pigpio

import data

def up():
    '''
    Function used to lift the pod.
    '''
    # The if statement is there to prevent the motor to go up if it's already in the up position
    # It avoid to beat up the poor limit switch
    if GPIO.input(data.switchUpPin):
        GPIO.output(data.motorDirPin, True) # set motor to go up
        pwm.hardware_PWM(data.motorStepPin, data.motorFreq, data.motorDutyCycle)
        GPIO.wait_for_edge(data.switchUpPin, GPIO.FALLING) # opt arg timeout in ms
        pwm.hardware_PWM(data.motorStepPin, data.motorFreq, 0)
    return None

def down():
    '''
    Function used to lower the pod
    '''
    GPIO.output(data.motorDirPin, False) # set motor to go down
    pwm.hardware_PWM(data.motorStepPin, data.motorFreq, data.motorDutyCycle)
    GPIO.wait_for_edge(data.switchDownPin, GPIO.RISING) #opt arg timeout in ms
    pwm.hardware_PWM(data.motorStepPin, data.motorFreq, 0)
    return None

def off():
    '''
    Function used to deactivate the motor
    '''
    GPIO.output(data.motorEnablePin, True)
    return None

def init():
    '''
    Function used to initialize the motor with the proper parameters.
    '''
    # The library used to control the GPIO is pigpio
    # You need to launch the deamon first in the terminal
    # To launch the deamon: sudo pigpiod
    # to stop the deamon: sudo killall pigpiod

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # Direction of the motor: 1->up, 0->down 
    GPIO.setup(data.motorDirPin, GPIO.OUT)

    # Enable: 0->on , 1->off
    GPIO.setup(data.motorEnablePin, GPIO.OUT) 
    GPIO.output(data.motorEnablePin, False)
    # Reset: 0->, 1->on
    GPIO.setup(data.motorResetPin, GPIO.OUT)
    GPIO.output(data.motorResetPin, True)
    # Sleep: 0->, 1->on
    GPIO.setup(data.motorSleepPin, GPIO.OUT)
    GPIO.output(data.motorSleepPin, True)
    
    # Resolution (MS1, MS2, MS3): we use quarter
    # Full step: (0,0,0)
    # Half step: (1,0,0)
    # Quater step: (0,1,0)
    # Eighth step: (1,1,0)
    # Seixteenth step: (1,1,1)

    # MS1
    GPIO.setup(data.motorMS1Pin, GPIO.OUT)
    GPIO.output(data.motorMS1Pin, False)
    # MS2
    GPIO.setup(data.motorMS2Pin, GPIO.OUT)
    GPIO.output(data.motorMS2Pin, True)
    # MS3
    GPIO.setup(data.motorMS3Pin, GPIO.OUT)
    GPIO.output(data.motorMS3Pin, False)

    return None

# Step of the motor: PWM
pwm = pigpio.pi()
pwm.set_mode(data.motorStepPin, pigpio.OUTPUT)

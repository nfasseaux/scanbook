# ScanBook/Scripts/data.py

from enum import Enum
import tkinter as tk
import tkinter.font as tkFont

'''
Every pin defined in this file and any other of the 
ScanBook project is  the GPIO number and not the pin number.
'''

# Enum of all the possible state of the state machine
class State(Enum):
    INIT = 0
    STOP = 1
    START = 2
    INITSCAN = 3
    SCAN = 4
    NEXT = 5
    SAVE = 6

# Enum of all the possible event of the state machine
class Event(Enum):
    START = 0
    NEXT = 1
    END = 2
    NONE = 3
    MENU = 4
    SHUTDOWN = 5

##### Buttons Pins (not used anymore)
#buttonStartPin = 26
#buttonNextPin = 19
#buttonEndPin = 12

##### Limit Switch
switchUpPin = 21
switchDownPin = 20

##### Camera
# Camera multiplexer card
# 1-> camera1, 0-> camera2
muxPin = 16
# Camera resolution
# res: (1024, 768) (1920, 768)
# tps captured + iP en 1920, 1080 png 15s
# tps captured + iP en 1920, 1080 jpg 2.6s
# tps captured + iP en 2592, 1944 png 4.5s
camResX = 2592
camResY = 1944

##### LED
ledPin=13 #channel 1
ledFreq=1009 # <80kHz prime number to avoid flicker
ledDutyCycle=1000000 #duty: 0-1 000 000

##### Motor
# Frequency of the PWM send to the stepper driver
# 1000 Hz work well
motorFreq =1000
# Duty cycle of the motor PWM, range from 0 to 1 000 000
# It's basically a square wave at 1 kHz
motorDutyCycle = 500000
# set the direction of the motor: 1->UP, 0->DOWN
motorDirPin = 23 
# Pin to send the PWM
motorStepPin = 18
# set the precision of the step of the motor
# fullstep, halfstep,..
# set at the begin of the program
# see motor.py/init() for more info
# or best the datasheet
motorMS1Pin = 22
motorMS2Pin = 27
motorMS3Pin = 17
# enable
motorEnablePin = 5
# reset
motorResetPin = 6
# sleep
motorSleepPin = 26

#### GUI
# GUI button parameters
buttonHeight = 5
buttonWidth = 25
# GUI LED button parameters
buttonLightHeight = 50
buttonLightWidth = 50

##### Other variables
# current state variable for the SM
currState=State.START
# variable to keep track of the page number
pageNb = 0
# variable to store the book name
bookName = ""
# variable to store the book path (on the RPi)
bookPath = ""
# variable to store the page capture path
capturedPath = ""
# variable to store the traited page path
traitedPath = ""
# format for the captured and traited page
# png: +quality
# jpg: +tps to traite +size
imageFormat = ".jpg"
# acces rights to create directory
accessRights = 0o755
